package com.example.preexamenunidad1android;

import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText etNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre = findViewById(R.id.etNombre);
        Button btnEntrar = findViewById(R.id.btnEntrar);
        Button btnSalir = findViewById(R.id.btnSalir);

        btnEntrar.setOnClickListener(v -> {
            String nombre = etNombre.getText().toString();
            if (!nombre.isEmpty()) {
                int numRecibo = new Random().nextInt(1000) + 1;
                Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                intent.putExtra("nombre", nombre);
                intent.putExtra("numRecibo", numRecibo);
                startActivity(intent);
            } else {
                Toast.makeText(MainActivity.this, "Por favor ingresa tu nombre", Toast.LENGTH_SHORT).show();
            }
        });

        btnSalir.setOnClickListener(v -> finish());
    }
}
