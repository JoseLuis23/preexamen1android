package com.example.preexamenunidad1android;

public class ReciboNomina {
    private String nombre;
    private int horasExtra, puesto, horasNormales, numRecibo;
    private double porcentajeImpuesto;

    public ReciboNomina(int numRecibo, String nombre, int horasNormales, int horasExtra, int puesto, double porcentajeImpuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtra = horasExtra;
        this.puesto = puesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public double calcularPagoBase() {
        double pagoBase = 200;
        switch (puesto) {
            case 1:
                pagoBase += pagoBase * 0.20;
                break;
            case 2:
                pagoBase += pagoBase * 0.50;
                break;
            case 3:
                pagoBase += pagoBase * 1.00;
                break;
        }
        return pagoBase;
    }

    public double calcularSubtotal() {
        double pagoBase = calcularPagoBase();
        return (pagoBase * horasNormales) + (pagoBase * horasExtra * 2);
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * (porcentajeImpuesto / 100);
    }

    public double calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }





    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getHorasNormales() {
        return horasNormales;
    }

    public void setHorasNormales(int horasNormales) {
        this.horasNormales = horasNormales;
    }

    public int getHorasExtra() {
        return horasExtra;
    }

    public void setHorasExtra(int horasExtra) {
        this.horasExtra = horasExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }
}



