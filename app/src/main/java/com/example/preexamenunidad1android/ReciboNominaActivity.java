package com.example.preexamenunidad1android;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText txtHorasNormales, txtHorasExtra, txtPorcentajeImpuesto;
    private RadioGroup rgPuestos;
    private Button btnCalcular, btnRegresar, btnLimpiar;
    private TextView lblResultado, lblNombreTrabajador, lblNumRecibo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        txtHorasNormales = findViewById(R.id.txtHorasNormales);
        txtHorasExtra = findViewById(R.id.txtHorasExtra);
        txtPorcentajeImpuesto = findViewById(R.id.txtPorcentajeImpuesto);
        rgPuestos = findViewById(R.id.rgPuestos);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegrear);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        lblResultado = findViewById(R.id.lblResultado);
        lblNombreTrabajador = findViewById(R.id.lblNombreTrabajador);
        lblNumRecibo = findViewById(R.id.lblNumRecibo);

        String nombre = getIntent().getStringExtra("nombre");
        int numRecibo = getIntent().getIntExtra("numRecibo", 0);

        lblNombreTrabajador.setText("Nombre: " + nombre);
        lblNumRecibo.setText("Número de Recibo: " + numRecibo);

        btnCalcular.setOnClickListener(v -> {
            int horasNormales = Integer.parseInt(txtHorasNormales.getText().toString());
            int horasExtra = Integer.parseInt(txtHorasExtra.getText().toString());
            int puesto = getPuestoSeleccionado();
            double porcentajeImpuesto = Double.parseDouble(txtPorcentajeImpuesto.getText().toString());

            ReciboNomina reciboNomina = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto, porcentajeImpuesto);

            String resultado = "Subtotal: " + reciboNomina.calcularSubtotal() +
                    "\nImpuesto: " + reciboNomina.calcularImpuesto() +
                    "\nTotal: " + reciboNomina.calcularTotal();

            lblResultado.setText(resultado);
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(v -> {
            txtHorasNormales.setText("");
            txtHorasExtra.setText("");
            txtPorcentajeImpuesto.setText("");
            lblResultado.setText("");
            rgPuestos.check(R.id.rbAuxiliar);
        });




    }


    private int getPuestoSeleccionado() {
        int idSeleccionado = rgPuestos.getCheckedRadioButtonId();
        if (idSeleccionado == R.id.rbAuxiliar) {
            return 1;
        } else if (idSeleccionado == R.id.rbAlbanil) {
            return 2;
        } else if (idSeleccionado == R.id.rbIngObra) {
            return 3;
        } else {
            return 0;
        }
    }
}



